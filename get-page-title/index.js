module.exports = async function (browser, event) {
  console.log("event", event);
  
  if (!event.inputParameters.testTargetUrl) {
    throw 'Invalid event data!';
  }

  const page = await browser.newPage();
  await page.goto(event.inputParameters.testTargetUrl);

  const title = await page.title();

  await page.screenshot({path: process.env.SCREENSHOTS_PATH + '/screenshot-1.png'});
  await page.screenshot({path: process.env.SCREENSHOTS_PATH + '/screenshot-2.png'});
  await page.screenshot({path: process.env.SCREENSHOTS_PATH + '/screenshot-3.png'});

  return { pageTitle: title };
};

